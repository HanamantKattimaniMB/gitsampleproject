Pipes:

#Download the contents of "Harry Potter and the Goblet of fire" using the command line from here
wget https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt

#Print the first three lines in the book
head -3 'Harry Potter and the Goblet of Fire.txt'

#Print the last 10 lines in the book
tail -10 'Harry Potter and the Goblet of Fire.txt'


#How many times do the following words occur in the book?
 Harry          : grep -o -i Harry Harry\ Potter\ and\ the\ Goblet\ of\ Fire.txt | wc -l    ans=3172
 Ron            : grep -o -i Ron Harry\ Potter\ and\ the\ Goblet\ of\ Fire.txt | wc -l    ans=1358
 Hermione       : grep -o -i Hermione Harry\ Potter\ and\ the\ Goblet\ of\ Fire.txt | wc -l    ans=872
 Dumbledore     : grep -o -i Dumbledore Harry\ Potter\ and\ the\ Goblet\ of\ Fire.txt | wc -l    ans=59
 
 #Print lines from 100 through 200 in the book
 head -n 200 Harry\ Potter\ and\ the\ Goblet\ of\ Fire.txt | tail -100 
 
 
#How many unique words are present in the book?
 cat Harry\ Potter\ and\ the\ Goblet\ of\ Fire.txt |awk '{print}' |sort|uniq|wc -l  ans=6753
 
 
 
 
 Processes, ports

#List your browser's process ids (pid) and parent process ids(ppid)
    ps -ef |awk '{print $2,$3,$8}'
    
    19641 19558 /usr/lib/firefox/firefox
    19698 19558 /usr/lib/firefox/firefox
    19757 19558 /usr/lib/firefox/firefox
    ppid=19558
    pid=19641,19698,19757
     
    or 
     
    pidof firefox command gives  19757 19698 19641 19558


#Stop the browser application from the command line
kill 19558


#List the top 3 processes by CPU usage.
ps -eo pid,ppid,cmd,%cpu --sort=-%cpu | head -4

  PID  PPID CMD                         %CPU
20997  1233 /usr/lib/firefox/firefox    22.5
18273 18272 /opt/zoom/zoom zoommtg://us 13.8
 4794  4719 /usr/share/discord/Discord  13.4




#List the top 3 processes by memory usage.
ps -eo pid,ppid,cmd,%mem --sort=-%mem | head -4
t
  PID  PPID CMD                         %MEM
20997  1233 /usr/lib/firefox/firefox    10.4
18273 18272 /opt/zoom/zoom zoommtg://us 10.4
 4794  4719 /usr/share/discord/Discord   9.7

#Start a Python HTTP server on port 8000
python3 -m http.server

#Open another tab. Stop the process you started in the previous step
Ctrl+z

#Start a Python HTTP server on port 90
 sudo python -m SimpleHTTPServer 90


#Display all active connections and the corresponding TCP / UDP ports.
$ netstat -natp | awk '{print $1,$5}'



#Find the pid of the process that is listening on port 5432
 netstat -a | grep ":5432"



#Use apt (Ubuntu) or homebrew (Mac) to:

    Install htop, vim and nginx
    sudo apt-get install htop
    sudo apt-get install vim
    sudo apt-get install nginx
    
    Uninstall nginx
    sudo apt-get remove nginx
    
    
    
Misc

#What's your local IP address?
ifconfig
192.168.43.223

#Find the IP address of google.com
ping google.com
2404:6800:4009:807::200e

or
nslookup google.com    (command)
Ip address of google.com is  172.217.174.78

#How to check if Internet is working using CLI?
ping googl.com



#Where is the node command located? What about code?
vs code and node js installed installed
